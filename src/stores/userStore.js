import { observable, action, computed } from 'mobx';
import agent from '../agent';

class UserStore {

  @observable currentUser;
  @observable loadingUser;
  @observable updatingUser;
  @observable updatingUserErrors;
  @observable isUser;

  @action pullUser() {
    this.loadingUser = true;
    return agent.Auth.current()
      .then(action((user) => {
        const role = this.isUser ? "USER" : "ADMIN";
        this.currentUser = {...user.user_info, roles: [role]} 
      }))
      .finally(action(() => { this.loadingUser = false; }))
  }

  @action updateUser(newUser) {
    this.updatingUser = true;
    return agent.Auth.save(newUser)
      .then(action((user) => { this.currentUser = user.user_info; }))
      .finally(action(() => { this.updatingUser = false; }))
  }

  @action setUser(newUser) {
    this.currentUser = newUser;
  }

  @action forgetUser() {
    this.currentUser = undefined;
  }

  @computed get userRoles () {
    if (this.currentUser) return this.currentUser.roles;
    return [];
  }

  @action setIsUser (isUser) {
    this.isUser = isUser
  }
}

export default new UserStore();
