import superagentPromise from "superagent-promise";
import _superagent from "superagent";
import commonStore from "./stores/commonStore";
import authStore from "./stores/authStore";

const superagent = superagentPromise(_superagent, global.Promise);

const API_ROOT = "http://mock.fali4.club";
// const API_ROOT = "http://fali4.club";

const encode = encodeURIComponent;

const handleErrors = err => {
  if (err && err.response && err.response.status === 401) {
    authStore.logout();
  }
  return err;
};

const responseBody = res => res.body;

const tokenPlugin = req => {
  if (commonStore.token) {
    req.set("authorization", `Token ${commonStore.token}`);
  }
};

const requests = {
  del: url =>
    superagent
      .del(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  get: url =>
    superagent
      .get(`${API_ROOT}${url}`)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  put: (url, body) =>
    superagent
      .put(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody),
  post: (url, body) =>
    superagent
      .post(`${API_ROOT}${url}`, body)
      .use(tokenPlugin)
      .end(handleErrors)
      .then(responseBody)
};

const Auth = {
  current: () => requests.get("/users"),
  login: (email, password) =>
    requests.post("/users/login", { user: { email, password } }),
  register: user => requests.post("/users", { user })
  // save: user =>
  //   requests.put('/user', { user })
};

const limit = (count, p) => `limit=${count}&offset=${p ? p * count : 0}`;
const omitSlug = article => Object.assign({}, article, { slug: undefined });

const Events = {
  all: () => requests.get("/events"),
  get: id => requests.get(`/events/${id}`),
  create: data => requests.post(`/events`),
  update: (id, data) => requests.post(`/events/${id}`),
  delete: id => requests.del(`/events/${id}`)
};

const Surveys = {
  all: () => requests.get("/surveys"),
  get: id => requests.get(`/surveys/${id}`),
  create: data => requests.post(`/surveys`),
  update: (id, data) => requests.post(`/surveys/${id}`),
  delete: id => requests.del(`/surveys/${id}`)
};

const Users = {
  all: () => requests.get("/users/all"),
  get: id => requests.get(`/users/${id}`),
  create: data => requests.post(`/users`),
  update: (id, data) => requests.post(`/users/${id}`),
  delete: id => requests.del(`/users/${id}`)
};

export default {
  Auth,
  Users,
  Events,
  Surveys
};
