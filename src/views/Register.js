import { NavLink } from "react-router-dom";
import React from "react";
import { inject, observer } from "mobx-react";

@inject("authStore")
@observer
export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userData: {
        name: "",
        phone: "",
        email: "",
        password: "",
        dateOfBirth: "",
        sex: "",
        address: "",
        bloodType: null
      },
      GDPR: false
    };
  }
  componentWillUnmount() {
    this.props.authStore.reset();
  }

  handleSubmitForm = e => {
    e.preventDefault();
    this.props.authStore
      .register(this.state.userData)
      .then(() => this.props.history.replace("/"));
  };

  handleDataChange = (name, value) => {
    this.setState({
      userData: {
        ...this.state.userData,
        [name]: value
      }
    });
  };

  handleOtherChange = (name, value) => {
    this.setState({
      [name]: value
    });
  };

  render() {
    const { errors, inProgress } = this.props.authStore;
    const { userData } = this.state;

    return (
      <section className="is-fullheight ImageBg">
        <div className="container">
          <div className="column is-6 is-offset-3">
            <h3 className="title has-text-white">Registracija</h3>
            <p className="subtitle has-text-grey">
              Registrirajte se za nastavak
            </p>
            <div className="box">
              <form onSubmit={this.handleSubmitForm}>
                <div className="field">
                  <label className="label">Ime i prezime</label>
                  <div className="control">
                    <input
                      className="input is-large"
                      type="text"
                      placeholder="Ime i prezime"
                      value={userData.name}
                      onChange={e =>
                        this.handleDataChange("name", e.target.value)
                      }
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <label className="label">Adresa</label>
                    <input
                      className="input is-large"
                      type="text"
                      placeholder="Adresa"
                      value={userData.address}
                      onChange={e =>
                        this.handleDataChange("address", e.target.value)
                      }
                    />
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="field">
                      <label className="label">Broj telefona</label>
                      <div className="control">
                        <input
                          className="input is-large"
                          type="number"
                          placeholder="Broj telefona"
                          value={userData.phone}
                          onChange={e =>
                            this.handleDataChange("phone", e.target.value)
                          }
                        />
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <div className="field">
                      <label className="label">Datum rođenja</label>
                      <div className="control">
                        <input
                          className="input is-large"
                          type="text"
                          placeholder="Datum rođenja"
                          value={userData.dateOfBirth}
                          onChange={e =>
                            this.handleDataChange("dateOfBirth", e.target.value)
                          }
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <div className="field">
                      <label className="label">Spol</label>
                      <div className="control">
                        <div className="select is-large">
                          <select
                            value={userData.sex}
                            className="select"
                            onChange={e =>
                              this.handleDataChange("sex", e.target.value)
                            }
                          >
                            <option value="M">Muško</option>
                            <option value="F">Žensko</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="column">
                    <div className="field">
                      <label className="label">Krvna grupa</label>
                      <div className="control">
                        <div className="select is-large">
                          <select
                            value={userData.bloodType}
                            className="select"
                            onChange={e =>
                              this.handleDataChange("bloodType", e.target.value)
                            }
                          >
                            <option value="0-">0-</option>
                            <option value="0+">0+</option>
                            <option value="A+">A+</option>
                            <option value="A+">A+</option>
                            <option value="B+">B+</option>
                            <option value="B+">B+</option>
                            <option value="AB+">AB+</option>
                            <option value="AB+">AB+</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field">
                  <label className="label">Email</label>
                  <div className="control">
                    <input
                      className="input is-large"
                      type="email"
                      placeholder="Email"
                      value={userData.email}
                      onChange={e =>
                        this.handleDataChange("email", e.target.value)
                      }
                    />
                  </div>
                </div>
                <div className="field">
                  <label className="label">Lozinka</label>
                  <div className="control">
                    <input
                      className="input is-large"
                      type="password"
                      placeholder="Lozinka"
                      value={userData.password}
                      onChange={e =>
                        this.handleDataChange("password", e.target.value)
                      }
                    />
                  </div>
                </div>
                <div className="box">
                  <h1>GDPR</h1>
                  <p>
                    Unutar naše aplikacije spremamo samo vaše podatke koje ste
                    nam dali prilikom registracije.
                  </p>
                  <br />
                  <p>
                    Koristit ćemo vaše podatke u svrhu poboljšanja usluge s
                    ciljem točnije procjene dolaska na događanje doniranja krvi.
                    Tako osiguravamo da krv koju prikupimo ne stoji predugo i
                    propadne.
                  </p>
                  <br />
                  <p>
                    Na zahtjev je moguce brisanje podataka o svakom korisniku
                  </p>
                  <br />
                  <label class="checkbox box">
                    <input
                      name="GDPR"
                      type="checkbox"
                      checked={this.state.GDPR}
                      onChange={e =>
                        this.handleOtherChange("GDPR", e.target.checked)
                      }
                    />{" "}
                    Prihvaćam uvjete i pravila
                  </label>
                  <br />
                  <label class="checkbox box">
                    <input
                      name="GDPR2"
                      type="checkbox"
                      checked={this.state.GDPR2}
                      onChange={e =>
                        this.handleOtherChange("GDPR2", e.target.checked)
                      }
                    />{" "}
                    Suglasan/na sam da se moji osobni podaci skladište u svrhu
                    korištenja aplikacije
                  </label>
                </div>
                <button
                  className="button is-block is-primary is-large is-fullwidth"
                  type="submit"
                  disabled={inProgress || !this.state.GDPR || !this.state.GDPR2}
                >
                  Napravi račun
                </button>
              </form>
            </div>
            <p className="has-text-grey">
              <NavLink to="/login">Već imate račun?</NavLink>
            </p>
          </div>
        </div>
      </section>
    );
  }
}
