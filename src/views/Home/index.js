import React from "react";
import { inject, observer } from "mobx-react";
import { withRouter, NavLink } from "react-router-dom";
import Logo from "../../assets/Logo.svg";

import {
  ResponsiveContainer,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Bar,
  BarChart,
  Legend
} from "recharts";

@inject("commonStore", "authStore")
@withRouter
@observer
export default class Home extends React.Component {
  render() {
    const { token, appName } = this.props.commonStore;
    const statistics = {
      performance: [
        {
          available: 60,
          min: 38,
          max: 78,
          bloodType: "0-"
        },
        {
          available: 240,
          min: 115,
          max: 240,
          bloodType: "0+"
        },
        {
          available: 52,
          min: 46,
          max: 96,
          bloodType: "A-"
        },
        {
          available: 155,
          min: 100,
          max: 210,
          bloodType: "A+"
        },
        {
          available: 48,
          min: 38,
          max: 82,
          bloodType: "B-"
        },
        {
          available: 43,
          min: 23,
          max: 50,
          bloodType: "B+"
        },
        {
          available: 9,
          min: 8,
          max: 18,
          bloodType: "AB-"
        },
        {
          available: 35,
          min: 16,
          max: 36,
          bloodType: "AB+"
        }
      ]
    };
    return (
      <div className="home-page">
        <section class="hero is-primary is-medium is-bold">
          <div class="hero-head">
            <nav class="navbar">
              <div class="container">
                <div class="navbar-brand">
                  <a class="navbar-item" href="/">
                    <img src={Logo} alt="Logo" /> Krv je život
                  </a>
                </div>
                <div className="navbar-end">
                  <div className="navbar-item">
                    <div className="field is-grouped">
                      <div className="control">
                        <NavLink to="/login" className="button is-link">
                          Prijava
                        </NavLink>
                        {/* <button  onClick={this.props.authStore.logout()}>
            Logout
          </button> */}
                      </div>
                      <div className="control">
                        <NavLink to="/register" className="button">
                          Registracija
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </nav>
          </div>
          <div class="hero-body">
            <div class="container has-text-centered">
              <h1 class="title">
                Darujte krv i pomozite nekome da ozdravi
              </h1>
              <h2 class="subtitle">
                Postanite darivatelj krvi i tako izravno pomozite pri
                ozdravljenju potrebitih te spašavanju tuđih života. Zajedno
                možemo više!
              </h2>
            </div>
          </div>
        </section>
        <div class="box cta">
          <p class="has-text-centered">
            <span class="tag is-warning">Citat dana</span> <i>"Potrebno je samo malo
            dobre volje i hrabrosti da spasimo tuđi život i pomognemo pri
            ozdravljenju"</i> &mdash; Tomisalv Glušić
          </p>
        </div>
        <section className="container">
          <div className="columns">
            <div className="column is-8 is-offset-2">
              <div style={{ textAlign: 'center', marginTop: 0 }}>
                <div className="list-title">Stanje količine krvi</div>
              </div>
              <ResponsiveContainer height={400} width="100%" style={{left: 0, right: 0, margin: 'auto'}}>
                <BarChart
                  data={statistics.performance}
                  margin={{
                    top: 40,
                    right: 30,
                    left: 20,
                    bottom: 5
                  }}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="bloodType" />
                  <YAxis />
                  <Tooltip />
                  <Legend />
                  <Bar
                    name="Minimalno"
                    dataKey="min"
                    stackId="a"
                    fill="#999999"
                  />
                  <Bar
                    name="Dostupno"
                    dataKey="available"
                    stackId="a"
                    fill="#ef415d"
                  />
                  <Bar
                    name="Maksimalno"
                    dataKey="max"
                    stackId="a"
                    fill="#e5e5e5"
                  />
                </BarChart>
              </ResponsiveContainer>
            </div>
          </div>
        </section>
        <section class="container">
          <div class="columns is-multiline">
            {[1, 2, 3].map(i => (
              <div class="column is-4">
                <div class="card is-shady">
                  <div class="card-image has-text-centered">
                    <i class="fa fa-paw" />
                  </div>
                  <div class="card-content">
                    <div class="content">
                      <h4>Akcija doniranja krivu Heinzlovoj</h4>
                      <p>
                        Sudjelujte i družite se s nama. Pomozite zajednici.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </section>
        <footer class="footer">
          <div class="container is-centered has-text-centered">
            Krv je život &copy; 2018
          </div>
        </footer>
      </div>
    );
  }
}
