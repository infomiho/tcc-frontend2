import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import Form from "./Form";

import agent from "../../../agent";

@withRouter
class Manage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }
  componentWillMount() {
    const id = this.props.match.params.id;
    agent.Surveys.get(id).then(data => {
      this.setState({
        data
      });
    });
  }
  update = data => {
    const id = this.props.match.params.id;
    agent.Surveys.update(id, data).then(() => {
      alert("success");
    });
  };
  render() {
    const { data } = this.state;
    return (
      <div className="Manage">
        <h1 className="title">Anketa</h1>
        <div className="Manage__Buttons">
          <button className="button is-danger">Obriši</button>
        </div>
        <div className="columns">
          <div className="column is-8">
            <div className="box">
              <Form data={data} onSubmit={this.update} />
            </div>
          </div>
          {/* <div className="column">
            <div className="box">desno</div>
          </div> */}
        </div>
      </div>
    );
  }
}

export default Manage;
