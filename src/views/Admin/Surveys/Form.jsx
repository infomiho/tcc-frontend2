import React, { Component } from "react";

import "react-datepicker/dist/react-datepicker.css";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        content: ""
      }
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.state.content !== '') return;
    this.setState({
      formData: {
        ...this.state.formData,
        ...nextProps.data
      }
    });
  }
  handleDataChange = (name, value) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [name]: value
      }
    });
  };
  render() {
    const { formData } = this.state;
    return (
      <form
        className="Form"
        onSubmit={() => this.props.onSubmit(this.state.formData)}
      >
        <div className="field">
          <label className="label">Sadržaj</label>
          <div className="control">
            <textarea
              className="textarea"
              type="text"
              placeholder="Sadržaj"
              value={formData.content}
              onChange={e => this.handleDataChange("content", e.target.value)}
            />
          </div>
        </div>
        <button className="button is-primary is-medium" type="submit">
          Spremi
        </button>
      </form>
    );
  }
}

export default Form;
