import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import filterList from "../../../utils/filterList";

import agent from "../../../agent";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      items: []
    };
  }
  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };
  all = data => {
    agent.Surveys.all().then(items => {
      this.setState({
        items
      });
    });
  };
  componentWillMount() {
    this.all();
  }
  render() {
    const { items } = this.state;
    const filteredList = filterList(this.state.search, items, 'content');

    return (
      <div className="List">
        <h1>Ankete</h1>
        <div className="List__Header">
          <div className="List__Header__Search">
            <input
              type="text"
              placeholder="Pretraga"
              onChange={this.onSearchChange}
              value={this.state.search}
            />
          </div>
          <div className="List__Header__Add">
            <NavLink className="List__Header__Button" to="/app/surveys/add">
              Dodaj
            </NavLink>
          </div>
        </div>
        <div className="List__Content">
          {filteredList.map(item => (
            <div className="ListItem">
              <div className="ListItem__Title">{item.content}</div>
              <div className="ListItem__Actions">
                <NavLink to={`/app/surveys/${item.id}`}>
                  Detalji <span className="entypo-right-open-big" />
                </NavLink>
              </div>
            </div>
          ))}
          {filteredList.length === 0 && (
            <div className="no-items left">
              Trenutno nema anketa za ovu pretragu.
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default List;
