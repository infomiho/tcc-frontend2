import React, { Component } from "react";
import QrReader from "react-qr-reader";

class QrScanner extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delay: 300,
      result: "No result"
    };
  }
  handleScan = data => {
    if (data) {
      this.setState({
        result: data
      });
    }
  };
  handleError(err) {
    console.error(err);
  }
  render() {
    return (
      <div className="QrScanner">
        <QrReader
          delay={this.state.delay}
          onError={this.handleError}
          onScan={this.handleScan}
          style={{ maxWidth: "300px", width: "100%", display: "inline-block" }}
        />
        <p>{this.state.result}</p>
      </div>
    );
  }
}

export default QrScanner;
