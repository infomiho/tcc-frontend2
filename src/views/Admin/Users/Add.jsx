import React, { Component } from "react";
import Form from "./Form";

import agent from '../../../agent';

class Add extends Component {
  create = data => {
    agent.Users.create(data).then(() => {
      alert('success');
    })
  }
  render() {
    return (
      <div className="columns">
        <div className="column is-8">
          <div className="box">
            <h1>Dodaj darivatelja</h1>
            <Form onSubmit={this.create}/>
          </div>
        </div>
      </div>
    );
  }
}

export default Add;
