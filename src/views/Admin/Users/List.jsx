import React, { Component } from "react";
import { NavLink } from "react-router-dom";

import filterList from "../../../utils/filterList";

import agent from "../../../agent";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      items: []
    };
  }
  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };
  all = data => {
    agent.Users.all().then(items => {
      this.setState({
        items
      });
    });
  };
  componentWillMount() {
    this.all();
  }
  render() {
    const { items } = this.state;

    const filteredList = filterList(this.state.search, items, 'name');

    return (
      <div className="List">
        <h1>Darivatelji</h1>
        <div className="List__Header">
          <div className="List__Header__Search">
            <input
              type="text"
              placeholder="Pretraga"
              onChange={this.onSearchChange}
              value={this.state.search}
            />
          </div>
          {/* <div className="List__Header__Add">
            <NavLink className="List__Header__Button" to="/app/users/add">
              Dodaj
            </NavLink>
          </div> */}
        </div>
        <div className="List__Content">
          {filteredList.map(item => (
            <div className="ListItem">
              <div className="ListItem__Title">{item.name}</div>
              <div className="ListItem__Actions">
                <NavLink to={`/app/users/${item.id}`}>
                  Detalji <span className="entypo-right-open-big" />
                </NavLink>
              </div>
            </div>
          ))}
          {filteredList.length === 0 && (
            <div className="no-items left">
              Trenutno nema darivatelja za ovu pretragu.
            </div>
          )}
        </div>
      </div>
    );
  }
}

export default List;
