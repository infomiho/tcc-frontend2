import React, { Component } from "react";

import "react-datepicker/dist/react-datepicker.css";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        name: "",
        phone: "",
        email: "",
        password: "",
        dateOfBirth: "",
        sex: "",
        address: "",
        bloodType: null
      }
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.state.name !== '') return;
    this.setState({
      formData: {
        ...this.state.formData,
        ...nextProps.data
      }
    });
  }
  handleDataChange = (name, value) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [name]: value
      }
    });
  };
  render() {
    const { formData } = this.state;
    return (
      <form className="Form" onSubmit={() => this.props.onSubmit(this.state.formData)}>
        <div className="field">
          <label className="label">Ime i prezime</label>
          <div className="control">
            <input
              className="input is-large"
              type="text"
              placeholder="Ime i prezime"
              value={formData.name}
              onChange={e => this.handleDataChange("name", e.target.value)}
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <label className="label">Adresa</label>
            <input
              className="input is-large"
              type="text"
              placeholder="Adresa"
              value={formData.address}
              onChange={e => this.handleDataChange("address", e.target.value)}
            />
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="field">
              <label className="label">Broj telefona</label>
              <div className="control">
                <input
                  className="input is-large"
                  type="number"
                  placeholder="Broj telefona"
                  value={formData.phone}
                  onChange={e => this.handleDataChange("phone", e.target.value)}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="field">
              <label className="label">Datum rođenja</label>
              <div className="control">
                <input
                  className="input is-large"
                  type="text"
                  placeholder="Datum rođenja"
                  value={formData.dateOfBirth}
                  onChange={e =>
                    this.handleDataChange("dateOfBirth", e.target.value)
                  }
                />
              </div>
            </div>
          </div>
        </div>
        <div className="columns">
          <div className="column">
            <div className="field">
              <label className="label">Spol</label>
              <div className="control">
                <div className="select is-large">
                  <select
                    value={formData.sex}
                    className="select"
                    onChange={e => this.handleDataChange("sex", e.target.value)}
                  >
                    <option value="M">Muško</option>
                    <option value="F">Žensko</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div className="column">
            <div className="field">
              <label className="label">Krvna grupa</label>
              <div className="control">
                <div className="select is-large">
                  <select
                    value={formData.bloodType}
                    className="select"
                    onChange={e =>
                      this.handleDataChange("bloodType", e.target.value)
                    }
                  >
                    <option value="0-">0-</option>
                    <option value="0+">0+</option>
                    <option value="A+">A+</option>
                    <option value="A+">A+</option>
                    <option value="B+">B+</option>
                    <option value="B+">B+</option>
                    <option value="AB+">AB+</option>
                    <option value="AB+">AB+</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="field">
          <label className="label">Email</label>
          <div className="control">
            <input
              className="input is-large"
              type="email"
              placeholder="Email"
              value={formData.email}
              onChange={e => this.handleDataChange("email", e.target.value)}
            />
          </div>
        </div>
        <div className="field">
          <label className="label">Lozinka</label>
          <div className="control">
            <input
              className="input is-large"
              type="password"
              placeholder="Lozinka"
              value={formData.password}
              onChange={e => this.handleDataChange("password", e.target.value)}
            />
          </div>
        </div>
        <button className="button is-primary is-medium" type="submit">
          Spremi
        </button>
      </form>
    );
  }
}

export default Form;
