import React, { Component } from "react";
import LocationInput from "../../../components/LocationInput";
import DatePicker from "react-datepicker";
import moment from "moment";

import "react-datepicker/dist/react-datepicker.css";

class Form extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData: {
        name: "",
        location: "",
        description: "",
        date_time: moment()
      }
    };
  }
  componentWillReceiveProps(nextProps) {
    if (this.state.name !== '') return;
    this.setState({
      formData: {
        ...this.state.formData,
        ...nextProps.data
      }
    });
  }
  handleDataChange = (name, value) => {
    this.setState({
      formData: {
        ...this.state.formData,
        [name]: value
      }
    });
  };
  render() {
    const { formData } = this.state;
    return (
      <form
        className="Form"
        onSubmit={() => this.props.onSubmit(this.state.formData)}
      >
        <div className="columns">
          <div className="column">
            <div className="field">
              <label className="label">Naziv</label>
              <div className="control">
                <input
                  className="input is-medium"
                  type="text"
                  placeholder="Naziv"
                  value={formData.name}
                  onChange={e => this.handleDataChange("name", e.target.value)}
                />
              </div>
            </div>
          </div>
          <div className="column">
            <div className="field">
              <label className="label">Lokacija</label>
              <div className="control">
                {/* <input
                  className="input is-large"
                  type="text"
                  placeholder="Lokacija"
                  value={formData.location}
                  onChange={e =>
                    this.handleDataChange("location", e.target.value)
                  }
                /> */}
                <LocationInput />
              </div>
            </div>
          </div>
        </div>
        <div className="field">
          <label className="label">Opis</label>
          <div className="control">
            <textarea
              className="textarea is-medium"
              type="text"
              placeholder="Opis"
              rows={4}
              value={formData.description}
              onChange={e =>
                this.handleDataChange("description", e.target.value)
              }
            />
          </div>
        </div>
        <div className="columns">
          <div className="column is-8">
            <div className="field">
              <label className="label">Datum i vrijeme</label>
              <div className="control">
                {/* <input
                  className="input is-large"
                  type="text"
                  placeholder="Datum"
                  value={formData.date}
                  onChange={e => this.handleDataChange("date", e.target.value)}
                /> */}
                <DatePicker
                  selected={formData.date_time}
                  onChange={e => this.handleDataChange("date_time", e)}
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={15}
                  dateFormat="DD. MM. YYYY. HH:mm"
                  timeCaption="time"
                  className="input is-medium"
                />
              </div>
            </div>
          </div>
          {/* <div className="column">
            <div className="field">
              <label className="label">Vrijeme</label>
              <div className="control">
                <input
                  className="input is-large"
                  type="text"
                  placeholder="Vrijeme"
                  value={formData.time}
                  onChange={e => this.handleDataChange("time", e.target.value)}
                />
              </div>
            </div>
          </div> */}
        </div>
        <button className="button is-primary is-medium" type="submit">
          Spremi
        </button>
      </form>
    );
  }
}

export default Form;
