import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import agent from "../../../agent";

import Form from "./Form";

import { ResponsiveContainer, Legend, PieChart, Pie, Cell } from "recharts";

const COLORS = [
  "#ef415d",
  "#3f3e3f",
  "#FFDC66",
  "#26A2BB",
  "#42d9c8",
  "#d62246",
  "#e3b505"
];

const colorForIndex = index => {
  return COLORS[index % COLORS.length];
};

class Manage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {}
    };
  }
  componentWillMount() {
    const id = this.props.match.params.id;
    agent.Events.get(id).then(data => {
      this.setState({
        data
      });
    });
  }
  handleDataChange = (name, value) => {
    this.setState({
      userData: {
        ...this.state.userData,
        [name]: value
      }
    });
  };
  update = data => {
    const id = this.props.match.params.id;
    agent.Events.update(id, data).then(() => {
      alert("success");
    });
  };
  render() {
    const { data } = this.state;
    const statistics = {
      response: [
        {
          name: "1 darivatelj",
          value: 312
        },
        {
          name: "2 darivatelja",
          value: 327
        },
        {
          name: "3 darivatelja",
          value: 86
        },
        {
          name: "4 darivatelja",
          value: 20
        }
      ],
    };

    const items1 = [
      { title: "Ivan Goran Kovačić", status: "pending", participated: true, id: 1 },
      { title: "Stjepan Mesić", status: "accepted", participated: false, id: 2 }
    ];
    const statusMapping = {
      pending: "Čekamo odgovor",
      rejected: "Odbio poziv",
      accepted: "Prihvatio poziv"
    };
    return (
      <div className="Manage">
        <h1 className="title">Akcija</h1>
        <div className="Manage__Buttons">
          <button className="button is-primary">Pošalji poziv</button>
          <NavLink className="button is-primary" to="/app/events/1/scan">
            Evidentiraj dolaznost
          </NavLink>
          <button className="button is-danger">Obriši</button>
        </div>
        <div className="columns">
          <div className="column is-8">
            <div className="box">
              <Form data={data} onSubmit={this.update} />
            </div>
          </div>
          <div className="column">
            <div className="box">
              <h2 className="subtitle">Odaziv darivatelja</h2>
              <ResponsiveContainer height={300} width="100%">
                <PieChart onMouseEnter={this.onPieEnter}>
                  <Pie
                    data={statistics.response}
                    innerRadius={60}
                    outerRadius={80}
                    fill="#333"
                    paddingAngle={5}
                    label
                  >
                    {statistics.response.map((entry, index) => (
                      <Cell fill={colorForIndex(index)} />
                    ))}
                  </Pie>
                  <Legend />
                </PieChart>
              </ResponsiveContainer>
            </div>
          </div>
        </div>
        <div className="UserList">
          <div className="list-title">Darivatelji</div>
          <div className="columns is-multiline">
            {items1.map(item => (
              <div className="column is-4">
                <div className="UserItem">
                  <div className="UserItem__Title">{item.title}</div>
                  <div className="UserItem__Status">
                    <div
                      className={`UserItem__Status__Left UserItem__Status__Left--${
                        item.status
                      }`}
                    >
                      {statusMapping[item.status]}
                    </div>
                    <div className="UserItem__Status__Right">
                      <NavLink to={`/app/users/${item.id}`}>
                        Detalji <span className="entypo-right-open-big" />
                      </NavLink>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Manage;
