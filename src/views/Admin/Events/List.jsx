import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import filterList from "../../../utils/filterList";
import agent from '../../../agent';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: "",
      items: [],
    };
  }
  onSearchChange = e => {
    this.setState({
      search: e.target.value
    });
  };
  all = () => {
    console.log('Called all')
    agent.Events.all().then(items => {
      this.setState({
        items
      })
    })
  }
  componentWillMount() {
    this.all();
  }
  render() {
    const { items } = this.state;

    const filteredFuture = filterList(this.state.search, items, 'name');
    // const filteredPast = filterList(this.state.search, items2);

    return (
      <div className="List">
        <h1>Akcije</h1>
        <div className="List__Header">
          <div className="List__Header__Search">
            <input
              type="text"
              placeholder="Pretraga"
              onChange={this.onSearchChange}
              value={this.state.search}
            />
          </div>
          <div className="List__Header__Add">
            <NavLink className="List__Header__Button" to="/app/events/add">
              Dodaj
            </NavLink>
          </div>
        </div>
        <div className="List__Content">
          <div className="list-title">Buduće akcije</div>
          {filteredFuture.map(item => (
            <div className="ListItem">
              <div className="ListItem__Title">{item.name}</div>
              <div className="ListItem__Actions">
                <NavLink to={`/app/events/${item.id}`}>
                  Detalji <span className="entypo-right-open-big" />
                </NavLink>
              </div>
            </div>
          ))}
          {filteredFuture.length === 0 && (
            <div className="no-items left">
              Trenutno nema akcija za ovu pretragu.
            </div>
          )}
        </div>
        {/* <div className="List__Content">
          <div className="list-title">Završene akcije</div>
          {filteredPast.map(item => (
            <div className="ListItem">
              <div className="ListItem__Title">{item.title}</div>
              <div className="ListItem__Actions">
                <NavLink to={`/app/events/${item.id}`}>
                  Detalji <span className="entypo-right-open-big" />
                </NavLink>
              </div>
            </div>
          ))}
          {filteredPast.length === 0 && (
            <div className="no-items left">
              Trenutno nema akcija za ovu pretragu.
            </div>
          )}
        </div> */}
      </div>
    );
  }
}

export default List;
