import React, { Component } from "react";
import Form from "./Form";
import agent from '../../../agent';

class Add extends Component {
  create = data => {
    agent.Events.create(data).then(() => {
      alert('success');
    })
  }
  render() {
    return (
      <div className="columns">
        <div className="column is-8">
          <div className="box">
            <h1>Dodaj akciju</h1>
            <Form onSubmit={this.create}/>
          </div>
        </div>
      </div>
    );
  }
}

export default Add;
