import { withRouter, NavLink } from "react-router-dom";
import React from "react";
import { inject, observer } from "mobx-react";

@inject("authStore", "userStore")
@withRouter
@observer
export default class Login extends React.Component {
  componentWillUnmount() {
    this.props.authStore.reset();
  }

  handleEmailChange = e => this.props.authStore.setEmail(e.target.value);
  handlePasswordChange = e => this.props.authStore.setPassword(e.target.value);
  handleSubmitForm = e => {
    e.preventDefault();
    this.props.authStore.login().then(() => {
      this.props.history.replace("/app");
    });
  };

  render() {
    const { values, errors, inProgress } = this.props.authStore;

    return (
      <section className="is-fullheight ImageBg">
        <div className="container">
          <div className="column is-6 is-offset-3">
            <h3 className="title has-text-white">Prijava</h3>
            <p className="subtitle has-text-grey">Prijavite se za nastavak</p>
            <div className="box">
              <form onSubmit={this.handleSubmitForm}>
                <div className="field">
                  <label className="label">Email</label>
                  <div className="control">
                    <input
                      className="input is-large"
                      type="email"
                      placeholder="Email"
                      value={values.email}
                      onChange={this.handleEmailChange}
                    />
                  </div>
                </div>

                <div className="field">
                  <label className="label">Lozinka</label>
                  <div className="control">
                    <input
                      className="input is-large"
                      type="password"
                      placeholder="Lozinka"
                      value={values.password}
                      onChange={this.handlePasswordChange}
                    />
                  </div>
                </div>
                <button
                  className="button is-block is-primary is-large is-fullwidth"
                  type="submit"
                  disabled={inProgress}
                >
                  Prijava
                </button>
              </form>
            </div>
            <p className="has-text-grey">
              <NavLink to="/register">Nemate račun?</NavLink>
            </p>
          </div>
        </div>
      </section>
    );
  }
}
