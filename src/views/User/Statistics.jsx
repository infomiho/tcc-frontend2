import React from "react";

import {
  ResponsiveContainer,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Bar,
  BarChart,
  Legend,
  PieChart,
  Pie,
  Cell
} from "recharts";

const COLORS = [
  "#26A2BB",
  "#3f3e3f",
  "#FFDC66",
  "#26A2BB",
  "#42d9c8",
  "#d62246",
  "#e3b505"
];

const colorForIndex = index => {
  return COLORS[index % COLORS.length];
};

class Statistics extends React.Component {
  render() {
    const statistics = {
      response: [
        {
          name: "Odazvani",
          value: 464
        },
        {
          name: "Neodazvani",
          value: 82
        }
      ],
      performance: [
        {
          available: 60,
          min: 38,
          max: 78,
          bloodType: "0-"
        },
        {
          available: 240,
          min: 115,
          max: 240,
          bloodType: "0+"
        },
        {
          available: 52,
          min: 46,
          max: 96,
          bloodType: "A-"
        },
        {
          available: 155,
          min: 100,
          max: 210,
          bloodType: "A+"
        },
        {
          available: 48,
          min: 38,
          max: 82,
          bloodType: "B-"
        },
        {
          available: 43,
          min: 23,
          max: 50,
          bloodType: "B+"
        },
        {
          available: 9,
          min: 8,
          max: 18,
          bloodType: "AB-"
        },
        {
          available: 35,
          min: 16,
          max: 36,
          bloodType: "AB+"
        }
      ]
    };
    return (
      <div className="Overview">
        <h1>Statistika</h1>
        <div>
          {statistics && [
            <div style={{ textAlign: 'center', marginTop: 40 }}>
              <div className="list-title">Stanje količine krvi</div>
            </div>,
            <section className="section">
              <ResponsiveContainer height={400} width="100%">
                <BarChart
                  data={statistics.performance}
                  margin={{
                    top: 0,
                    right: 0,
                    left: 0,
                    bottom: 5
                  }}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="bloodType" />
                  <YAxis />
                  <Tooltip />
                  <Legend />
                  <Bar
                    name="Minimalno"
                    dataKey="min"
                    stackId="a"
                    fill="#999999"
                  />
                  <Bar
                    name="Dostupno"
                    dataKey="available"
                    stackId="a"
                    fill="#ef415d"
                  />
                  <Bar
                    name="Maksimalno"
                    dataKey="max"
                    stackId="a"
                    fill="#e5e5e5"
                  />
                </BarChart>
              </ResponsiveContainer>
            </section>,
            <div style={{ textAlign: 'center', marginTop: 40 }}>
              <div className="list-title">Odaziv darivatelja ovaj mjesec</div>
            </div>,
            <section className="section">
              <ResponsiveContainer height={300} width="100%">
                <PieChart onMouseEnter={this.onPieEnter}>
                  <Pie
                    data={statistics.response}
                    innerRadius={60}
                    outerRadius={80}
                    fill="#333"
                    paddingAngle={5}
                    label
                  >
                    {statistics.response.map((entry, index) => (
                      <Cell fill={colorForIndex(index)} />
                    ))}
                  </Pie>
                  <Legend />
                </PieChart>
              </ResponsiveContainer>
            </section>
          ]}
        </div>
      </div>
    );
  }
}

export default Statistics;
