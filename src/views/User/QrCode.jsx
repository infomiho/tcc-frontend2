import React, { Component } from "react";
import QrComponent from "qrcode.react";
import { inject } from "mobx-react";

@inject("userStore")
class QrCode extends Component {
  render() {
    const { currentUser } = this.props.userStore;
    console.log(currentUser);
    return (
      <div className="QrCode">
        <h1>Moj QR kod</h1>
        <p>Vaš QR kod služi radi lakše evidencije vašeg doprinosa.</p>
        <div className="QrCode__Content">
          <div className="box" style={{ display: 'inline-block' }}>
            {currentUser && <QrComponent value={currentUser.email} size={300} />}
          </div>
        </div>
      </div>
    );
  }
}

export default QrCode;
