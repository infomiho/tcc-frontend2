import React from 'react';
import QuestionCard from "../../components/QuestionCard";
import Survey from "../../components/Survey";
import Message from "../../components/Message";

class Overview extends React.Component {
  render() {
    return (
      <div className="Overview">
        <h1>Pregled</h1>
        <div className="container x-small">
          <Survey
            text="Jeste li bili u Ujedinjenom Kraljevstvu u proteklih 2mj.?"
          />
          <QuestionCard
            title="Doniranje krvi u Brozovoj"
            location="Brozova uica 34/2"
            time={new Date()}
            message="Ova se dobrovoljna akcija i  trebamo vašu pomoć kako bismo ju realizirali"
            isComing={true}
          />

          <QuestionCard
            title="Doniranje krvi"
            location="Heinzlova ulica 34/2"
            time={new Date()}
            message="Molimo sve pozvane da se odazovu kako bismo što prije prikupili kvotu"
            isComing={false}
          />

          <Message
            title="Odgoda doniranja 25. 5."
            message="Odgađa se doniranje u petak jer nismo uspjeli skupiti dovljno ljudi"
          />
        </div>
      </div>
    );
  }
}

export default Overview;