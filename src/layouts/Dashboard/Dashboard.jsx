import React from "react";

import UserOverview from "../../views/User/Overview";
import UserStatistics from "../../views/User/Statistics";
import UserEvents from "../../views/User/Events";
import UserQrCode from "../../views/User/QrCode";

import AdminStatistics from "../../views/Admin/Statistics";
import AdminEventsList from "../../views/Admin/Events/List";
import AdminEventsManage from "../../views/Admin/Events/Manage";
import AdminEventsAdd from "../../views/Admin/Events/Add";

import AdminMessages from "../../views/Admin/Messages";

import AdminUsersList from "../../views/Admin/Users/List";
import AdminUsersManage from "../../views/Admin/Users/Manage";
import AdminUsersAdd from "../../views/Admin/Users/Add";

import AdminSurveyList from "../../views/Admin/Surveys/List";
import AdminSurveyManage from "../../views/Admin/Surveys/Manage";
import AdminSurveyAdd from "../../views/Admin/Surveys/Add";

import AdminQrScanner from "../../views/Admin/QrScanner";

import RoleGuard from "../../components/RoleGuard";

import { Switch, Route, NavLink, Redirect } from "react-router-dom";
import Sidebar from "../../components/Sidebar/Sidebar";
import { inject, observer } from "mobx-react";
import MenuIcon from "../../assets/mbri-menu.svg";
import QRCodeIcon from "../../assets/qr-code.svg";

import Logo from "../../assets/Logo.svg";

@inject("userStore", "commonStore")
@observer
class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileOpen: false,
      loading: true
    };
  }

  componentDidMount() {
    this.props.userStore
      .pullUser()
      .then(() => this.setState({ loading: false }));
  }

  handleDrawerToggle = () => {
    this.setState({ mobileOpen: !this.state.mobileOpen });
  };

  renderRoutes = user => {
    if (user && user.roles.includes("ADMIN")) {
      return (
        <Switch>
          <Redirect from="/app" to="/app/statistics" exact />
          <Route exact path="/app/statistics" component={AdminStatistics} />
          <Route exact path="/app/messages" component={AdminMessages} />
          <Route exact path="/app/events" component={AdminEventsList} />
          <Route path="/app/events/add" component={AdminEventsAdd} />
          <Route exact path="/app/events/:id" component={AdminEventsManage} />
          <Route path="/app/events/:id/scan" component={AdminQrScanner} />
          <Route exact path="/app/users" component={AdminUsersList} />
          <Route path="/app/users/add" component={AdminUsersAdd} />
          <Route path="/app/users/:id" component={AdminUsersManage} />
          <Route exact path="/app/surveys" component={AdminSurveyList} />
          <Route exact path="/app/surveys/add" component={AdminSurveyAdd} />
          <Route exact path="/app/surveys/:id" component={AdminSurveyManage} />
        </Switch>
      );
    }
    return (
      <Switch>
        <Route exact path="/app" component={UserOverview} />
        <Route exact path="/app/statistics" component={UserStatistics} />
        <Route exact path="/app/events" component={UserEvents} />
        <Route exact path="/app/qr-code" component={UserQrCode} />
      </Switch>
    );
  };

  render() {
    if (this.state.loading)
      return (
        <div className="LoaderWrapper">
          <div className="Loader" />
        </div>
      );

    return (
      <div
        className={`Dashboard${
          this.state.mobileOpen ? " Dashboard--open" : ""
        }`}
      >
        <div className="Navbar">
          <div className="Navbar__Toggle" onClick={this.handleDrawerToggle}>
            <img src={MenuIcon} alt="" />
          </div>
          <div className="Navbar__Logo">
            <img src={Logo} alt="" />
          </div>
          <div className="Navbar__Title">
            <RoleGuard role="USER">
              <NavLink to="/app/qr-code" className="qr-button">
                <img src={QRCodeIcon} />
              </NavLink>
            </RoleGuard>
          </div>
        </div>
        <Sidebar
          handleDrawerToggle={this.handleDrawerToggle}
          open={this.state.mobileOpen}
        />
        <div className="app-content">
          {this.renderRoutes(this.props.userStore.currentUser)}
        </div>
      </div>
    );
  }
}

export default Dashboard;
