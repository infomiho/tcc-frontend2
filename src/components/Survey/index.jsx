import React from "react";

class Survey extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      answered: false,
    }
  }
  render() {
    const { text } = this.props;
    const { answered } = this.state;

    if (answered) return (
      <div className="Survey overview-card">
        <div className="survey-content">
          <div className="text">Zahvaljujemo na odgovoru!</div>
        </div>
      </div>
    )

    return (
      <div className="Survey overview-card">
        <div className="card-title">Upitnik</div>
        <div className="survey-content">
          <div className="text">{text}</div>
          <div className="options">
            <button onClick={() => this.setState({ answered: true })}>Da</button>
            <button onClick={() => this.setState({ answered: true })}>Ne</button>
          </div>
        </div>
      </div>
    )
  }
}

export default Survey;
