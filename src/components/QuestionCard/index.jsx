import React from "react";
import formatDate from "../../utils/formatDate";

class QuestionCard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isComing: this.props.isComing
    }
  }
  render() {
    const { title, location, time, message } = this.props;
    const { isComing } = this.state

    return (
      <div className="QuestionCard overview-card">
        <div className="header">
          <div className="card-title">Doniranje krvi</div>
          <div className="title">{title}</div>
        </div>
        <div className="info">
          <div className="info-section">
            <span className="entypo-location" />
            <a href="#">{location}</a>
          </div>
          <div className="info-section">
            <span className="entypo-clock" />
            <span>{formatDate(time)}</span>
          </div>
        </div>
        <div className="message">{message}</div>
        <button onClick={() => this.setState({ isComing: !isComing })} className={`isComing ${isComing ? 'cancel' : ''}`}>{ isComing ? 'Ipak ne mogu doći' : 'Dolazim' }</button>
      </div>
    )
  }
}

export default QuestionCard;
