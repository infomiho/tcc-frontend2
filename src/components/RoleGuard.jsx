import React from 'react';
import { inject, observer } from 'mobx-react';

@inject('userStore', 'commonStore')
@observer
export default class RoleGuard extends React.Component {
  render() {
    const { currentUser } = this.props.userStore;
    if (currentUser && currentUser.roles.includes(this.props.role)) return this.props.children;
    return null;
  }
}