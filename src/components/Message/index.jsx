import React from "react";
import formatDate from "../../utils/formatDate";

class Message extends React.Component {
  render() {
    const { title, message } = this.props;

    return (
      <div className="MessageCard overview-card">
        <div className="header">
          <div className="card-title">Poruka</div>
          <div className="title">{title}</div>
        </div>
        <div className="message">{message}</div>
      </div>
    )
  }
}

export default Message;
