import React from 'react';
import { Switch, Route, withRouter, Inde } from 'react-router-dom';
import { inject, observer } from 'mobx-react';

import Home from '../views/Home';
import Login from '../views/Login';
import Register from '../views/Register';
import Dashboard from '../layouts/Dashboard/Dashboard';

import PrivateRoute from '../components/PrivateRoute';
import PublicRoute from '../components/PublicRoute';

import DevTools from 'mobx-react-devtools';

import '../styles/main.css';

@inject('userStore', 'commonStore')
@withRouter
@observer
export default class App extends React.Component {

  componentWillMount() {
    if (!this.props.commonStore.token) {
      this.props.commonStore.setAppLoaded();
    }
  }

  componentDidMount() {
    if (this.props.commonStore.token) {
      this.props.userStore.pullUser()
        .finally(() => this.props.commonStore.setAppLoaded());
    }
  }

  render() {
    if (this.props.commonStore.appLoaded) {
      return (
        <div>
          <Switch>
            <PublicRoute path="/login" component={Login} />
            <PublicRoute path="/register" component={Register} />
            <Route path="/app" component={Dashboard} />
            {/* <Route path="/editor/:slug?" component={Editor} />
            <Route path="/article/:id" component={Article} />
            <PrivateRoute path="/settings" component={Settings} />
            <Route path="/@:username" component={Profile} />
            <Route path="/@:username/favorites" component={Profile} /> */}
            <Route path="/" component={Home} />
          </Switch>
          {/* <DevTools /> */}
        </div>
      );
    }
    return (
      null
    );
  }
}
