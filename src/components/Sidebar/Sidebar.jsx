import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import RoleGuard from '../../components/RoleGuard';
import { inject } from 'mobx-react';
import Logo from '../../assets/Logo.svg';
import QRCodeIcon from '../../assets/qr-code.svg';

@inject('authStore')
@withRouter
class Sidebar extends React.Component {
  render() {
    return (
      <div className="Sidebar">
        <div className="logo-container">
          <img className="logo" src={Logo} />
          <span className="title">Krv je život</span>
        </div>

        <ul className="page-links">
          <RoleGuard role="USER">
            <li>
              <NavLink exact to="/app">
                <span className="entypo-flash" />
                <span>Pregled</span>
              </NavLink>
            </li>
          </RoleGuard>
          <li>
            <NavLink exact to="/app/statistics">
              <span className="entypo-chart-line"></span>
              <span>Statistika</span>
            </NavLink>
          </li>
          <RoleGuard role="ADMIN">
            <li>
              <NavLink to="/app/events">
                <span className="entypo-calendar" />
                <span>Akcije</span>
              </NavLink>
            </li>
          </RoleGuard>
          <RoleGuard role="ADMIN">
            <li>
              <NavLink to="/app/users">
                <span className="entypo-user" />
                <span>Darivatelji</span>
              </NavLink>
            </li>
          </RoleGuard>
          <RoleGuard role="ADMIN">
            <li>
              <NavLink to="/app/surveys">
                <span className="entypo-doc-text" />
                <span>Ankete</span>
              </NavLink>
            </li>
          </RoleGuard>
          <RoleGuard role="USER">
            <li>
              <NavLink exact to="/app/qr-code">
                <img src={QRCodeIcon} className="image" />
                <span>Moj QR kod</span>
              </NavLink>
            </li>
          </RoleGuard>
          <li>
            <button onClick={() => this.props.authStore.logout().then(() => this.props.history.replace("/"))}>
              <span className="entypo-logout" />
              <span>Odjava</span>
            </button>
          </li>
        </ul>
      </div>
    );
  }
}

export default Sidebar;