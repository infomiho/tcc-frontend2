export default (search, items, key='title') =>
  items.filter(item => item[key].toLowerCase().includes(search.toLowerCase()));
