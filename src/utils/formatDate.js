import moment from 'moment';
export default date => {
    return moment(date).format('D. M. YYYY.')
}